require 'rails_helper'

describe "Deck" do

  let(:user) { create(:user) }

  before { sign_in(user) }

  describe 'create deck' do

    context 'successful creation' do
      it 'create deck' do
        click_on 'Создать колоду'
        fill_in 'Name', with: 'MyTestDeck2'
        click_button 'Создать'

        expect(page).to have_content 'MyTestDeck2'
      end
    end

    context 'unsuccessfully' do

      it 'name blank' do
        click_on 'Создать колоду'
        fill_in 'Name', with: ''
        click_button 'Создать'

        expect(page).to have_content 'Namecan\'t be blank'
      end
    end
  end

  describe 'update deck' do
    let(:deck) { create(:deck, user: user) }

    before { visit edit_deck_path(deck) }

    context 'successful update' do

      it 'make change Namecan' do
        fill_in 'Name', with: 'NewName'

        click_button 'Сохранить'
        expect(page).to have_content 'NewName'
      end
    end

    context 'unsuccessfully' do

      it 'name blank' do
        fill_in 'Name', with: ''

        click_button 'Сохранить'
        expect(page).to have_content 'Namecan\'t be blank'
      end
    end
  end
end
