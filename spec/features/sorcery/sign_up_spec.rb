require 'rails_helper'

describe "Sign up " do

  context do
    it "User can register" do
      visit root_path
      click_on 'Регистрация'

      fill_in 'Email', with: 'test123@gmail.com'
      fill_in 'Password', with: '12345'
      fill_in 'Password confirmation', with: '12345'
      click_on 'Создать'

      expect(page).to have_content "Вы зарегистрировались"
    end
  end
end
