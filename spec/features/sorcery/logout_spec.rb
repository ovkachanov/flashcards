require 'rails_helper'

describe "Logout " do
  let!(:user) { create(:user) }

  context do
    it "Logged in user can log out" do
      visit new_user_session_path
      fill_in 'Email', with: user.email
      fill_in 'Password', with: '12345678'
      click_on 'Войти'

      click_on 'Выход'

      expect(page).to have_content 'Выход выполнен!'
    end
  end
end
