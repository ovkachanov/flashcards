require 'rails_helper'

describe "Sign in  " do
  let!(:user) { create(:user) }

  context do
    it "Registered user can enter the site" do
      visit root_path
      click_on 'Вход'

      fill_in 'Email', with: user.email
      fill_in 'Password', with: '12345678'
      click_on 'Войти'

      expect(page).to have_content "Вход выполнен успешно!"
    end
  end
end
