require 'rails_helper'

describe "User" do

  describe "User can add card" do
      let! (:user) { create(:user) }
      let! (:deck) { create(:deck,  user: user) }

      context do

        it "Unregistered user does not see the link 'Add card'" do
          visit root_path

          expect(page).not_to have_content "Добавить карточку"
        end

        it "Registered & logiin user  see the link 'Add card'" do
          sign_in(user)
          expect(page).to have_content "Добавить карточку"
        end

        it "Authorized user can create cards" do
          sign_in(user)
          click_on 'Добавить карточку'

          fill_in 'Original text', with: 'Table'
          fill_in 'Translated text', with: 'Стол'
          click_on 'Создать'

          expect(page).to have_content "Карточка успешно создана!"
        end

        it "Authorized user can create deck" do
          sign_in(user)

          click_on 'Создать колоду'

          fill_in 'Name', with: 'New Deck'

          click_on 'Создать'

          expect(page).to have_content "Колода успешно создана!"
        end
      end
    end

    describe "User can delete card" do
      let!(:author) { create(:user) }
      let(:deck1) { create(:deck, user: author)}
      let(:card1) {create(:card, deck: deck1)}

      let!(:other_user) { create(:user) }
      let(:deck2) { create(:deck, user: other_user)}
      let(:card2) {create(:card, deck: deck2)}

      before { sign_in(author) }

      it "User can delete you card" do
        visit card_path(card1)

        expect(page).to have_content "Удалить карточку"
        click_on 'Удалить карточку'
        expect(page).not_to have_content card1.original_text
      end

      it "User can not remove someone else's card" do
        visit card_path(card2)

        expect(page).not_to have_content "Удалить карточку"
      end
    end

    describe "user can select the current deck" do
      let!(:author) { create(:user) }
      let!(:other_user) { create(:user) }

      let!(:deck1) { create(:deck, user: author)}
      let!(:deck2) { create(:deck, user: other_user)}

      before { sign_in(author) }

        it "can choose your deck" do
          visit decks_path
          click_on 'Сделать текущей'
          expect(author.current_deck) == deck1
        end

        it "can't choose someone else" do
          author.update_attribute(:current_deck, deck2)
          expect(author.current_deck) != deck2
        end
    end
end
