require 'rails_helper'

describe "User is on the main training page " do

  let(:user) { create(:user)}
  let(:deck) { create(:deck, user: user)}
  let!(:card) { create(:card, deck: deck) }
  before { sign_in(user) }

  context "User sees card on main page" do
    it "User sees original text" do
      expect(page).to have_content card.original_text
    end

    it "the user sees the picture belonging to the card" do
      expect(card.photo).to be_format ('jpeg')
      expect(card.photo).to have_dimensions(360, 360)
    end
  end

  context "sequence of correct answers" do
    it "correct starting conditions" do
      expect(card.box).to eq 0
      expect(card.check_attempt).to eq 0
    end

    it "the user gives the first correct answer" do
      state_setting_and_check(card, 0)

      expect(card.box).to eq 1
      expect(card.check_attempt).to eq 0
      expect(card.review_date).to be_within(1.second).of(Time.now + 12.hours)
    end

    it "the user gives the second correct answer" do
      state_setting_and_check(card, 1)

      expect(card.box).to eq 2
      expect(card.check_attempt).to eq 0
      expect(card.review_date).to be_within(1.second).of(Time.now + 3.days)
    end

    it "the user gives the third correct answer" do
      state_setting_and_check(card, 2)

      expect(card.box).to eq 3
      expect(card.check_attempt).to eq 0
      expect(card.review_date).to be_within(1.second).of(Time.now + 1.week)
    end

    it "the user gives the fourth correct answer" do
      state_setting_and_check(card, 3)

      expect(card.box).to eq 4
      expect(card.check_attempt).to eq 0
      expect(card.review_date).to be_within(1.second).of(Time.now + 2.weeks)
    end

    it "the user gives the fifth correct answer" do
      state_setting_and_check(card, 4)

      expect(card.box).to eq 5
      expect(card.check_attempt).to eq 0
      expect(card.review_date).to be_within(1.second).of(Time.now + 1.month)
    end
  end

  context "the user gives the wrong answer" do
    it "the user gives the first wrong answer" do
      state_setting_and_check(card, 3, 0, 'Машина')

      expect(card.box).to eq 3
      expect(card.check_attempt).to eq 1
    end

    it "the user gives the second wrong answer" do
      state_setting_and_check(card, 3, 1, 'Машина')

      expect(card.box).to eq 3
      expect(card.check_attempt).to eq 2
    end

    it "the user gives the third wrong answer" do
      state_setting_and_check(card, 3, 2, 'Машина')

      expect(card.box).to eq 3
      expect(card.check_attempt).to eq 3
    end

    it "the user gives the fourth wrong answer" do
      state_setting_and_check(card, 3, 3, 'Машина')

      expect(card.box).to eq 1
      expect(card.check_attempt).to eq 0
    end
  end
end
