require 'rails_helper'

describe 'Whenever Schedule' do
  let(:schedule) { schedule = Whenever::Test::Schedule.new(file: 'config/schedule.rb') }

  before do
    load 'Rakefile'
    schedule
  end

  it 'makes sure `runner` statements exist' do
    assert_equal 1, schedule.jobs[:runner].count
    schedule.jobs[:runner].each { |job| instance_eval job[:task] }
  end

  it 'makes sure runner start every day' do
    assert_equal [:day], schedule.jobs[:runner].first[:every]
  end
end
