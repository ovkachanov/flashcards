require 'rails_helper'

describe CardsController, type: :controller do
  let(:user) { create(:user) }
  let(:deck) { create(:deck, user: user) }
  let(:card) { create(:card, deck: deck) }
  before { login_user(user) }


  describe "GET #index" do
    before { get :index }

    it 'renders index' do
      expect(response).to render_template :index
    end
  end

  describe 'GET #show' do
    before { get :show, params: { id: card.id } }

    it 'assigns the requested card to @card' do
      expect(assigns(:card)).to eq card
    end

    it 'renders show' do
      expect(response).to render_template :show
    end
  end

  describe 'GET #new' do
    before { get :new }

    it 'renders new' do
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    it 'create card and save in database' do
      expect { card }.to change(Card, :count).by(1)
    end

    it 'redirect to card path if card valid' do
      post :create, params: { id: card.id, card: attributes_for(:card, deck_id: deck.id) }
      expect(response).to redirect_to card_path(assigns(:card))
    end
  end

  describe 'PATCH #update' do
    it 'changes the card attributes' do
      patch :update, params: { id: card.id, card: { original_text: 'create', translated_text: 'создавать' } }
      card.reload
      expect(card.original_text).to eq 'create'
      expect(card.translated_text).to eq 'создавать'
    end

    it 'assigns the requested card to @card' do
      patch :update, params: { id: card.id, card: { original_text: nil, translated_text: 'создавать' } }
      expect(response).to render_template :edit
    end
  end

  describe 'DELETE #destroy' do
    let!(:card) { create(:card, deck: deck) }
    let(:other_user) { create(:user) }

    context 'user can delete their cards' do
      it 'delete card' do
        expect { delete :destroy, params: { id: card.id} }.to change(deck.cards, :count).by(-1)
      end

      it 'redirects to cards#index' do
        delete :destroy, params: { id: card.id }
        expect(response).to redirect_to cards_path
      end
    end

    it 'user can not remove other cards' do
      logout_user
      login_user(other_user)
      expect { delete :destroy, params: {id: card.id} }.not_to change(Card, :count)
    end
  end

  describe 'POST #check_translation' do

    context 'correct input' do
      before { post :check_translation, params: { id: card.id, answer: "Стол" } }

      it 'redirect to root_path' do
        expect(response).to redirect_to root_path
      end

      it "flashes a notice message" do
       expect(flash[:notice]).to match("Правильно")
      end
    end

    context 'incorrect input' do
      it "flashes a notice message" do
       post :check_translation, params: { id: card.id, answer: "Машина" }
       expect(flash[:notice]).to match("Не правильно")
      end
    end
  end
end
