require 'rails_helper'

describe UsersController, type: :controller do
  let(:user) { create(:user) }
  before { login_user(user) }


  describe 'GET #new' do
    before { get :new }

    it 'assigns a new user' do
      expect(assigns(:user)).to be_a_new(User)
    end

    it 'renders new view' do
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    before { post :create, params: { id: user.id, user:  attributes_for(:user) } }

    it 'redirect to root path if user valid' do
      expect(response).to redirect_to root_path
    end

    it "flashes a notice message" do
     expect(flash[:notice]).to be_present
    end

    it "render new if invalid user " do
     post :create, params: { id: user.id, user:  { email: nil, password: '12345678'} }
     expect(response).to render_template :new
    end
  end

  describe 'PATCH #update' do
    it 'assigns the requested user to @user' do
      patch :update, params: { id: user.id, user: attributes_for(:user) }
      expect(assigns(:user)).to eq user
    end

    it 'changes the user attributes' do
      patch :update, params: { id: user.id, user: { email: 'oleg@test.com' } }
      user.reload
      expect(user.email).to eq 'oleg@test.com'
    end
  end

  describe 'DELETE #destroy' do
    it 'delete user' do
      expect { delete :destroy, params: { id: user.id} }.to change(User, :count).by(-1)
    end

    it 'redirects to root path' do
      delete :destroy, params: { id: user.id}
      expect(response).to redirect_to root_path
    end
  end
end
