require 'rails_helper'

describe DecksController, type: :controller do
  let(:user) { create(:user) }
  let(:deck) { create(:deck, user: user) }

  before { login_user(user) }

  describe "GET #index" do
    before { get :index }

    it 'renders index' do
      expect(response).to render_template :index
    end
  end

  describe 'GET #show' do
    before { get :show, params: { id: deck.id } }

    it 'assigns the requested deck to @deck' do
      expect(assigns(:deck)).to eq deck
    end

    it 'renders show' do
      expect(response).to render_template :show
    end
  end

  describe 'GET #new' do
    before { get :new }

    it 'renders new' do
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    it 'create deck and save in database' do
      expect { deck }.to change(Deck, :count).by(1)
    end

    it 'redirect to deck path if deck valid' do
      post :create, params: { id: deck.id, deck: attributes_for(:deck) }
      expect(response).to redirect_to deck_path(assigns(:deck))
    end
  end

  describe 'PATCH #update' do
    it 'changes the deck attributes' do
      patch :update, params: { id: deck.id, deck: { name: 'newdeck' } }
      deck.reload
      expect(deck.name).to eq 'newdeck'
    end

    it 'assigns the requested deck to @deck' do
      patch :update, params: { id: deck.id, deck: { name: nil } }
      expect(response).to render_template :edit
    end
  end

  describe 'PATCH #make_current' do
    let(:other_user) { create(:user) }
    let(:deck2) { create(:deck, user: other_user) }

    it 'can choose from their decks' do
      patch :make_current, params: { id: deck.id, user: user}
      expect(user.current_deck).to eq deck
    end

    it "can't choose from other people's decks" do
     patch :make_current, params: { id: deck2.id, user: user}
     expect(user.current_deck).to_not eq deck2
    end
  end
end
