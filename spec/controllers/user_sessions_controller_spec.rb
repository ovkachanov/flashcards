require 'rails_helper'

describe UserSessionsController, type: :controller do
  let(:user) { create(:user) }

  describe 'GET #new' do
    it 'returns success' do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #create' do
    it 'returns success' do
      get :create, params: { user: user }
      expect(response).to have_http_status(:success)
    end

    it 'returns renders #new' do
      get :create, params: { user: { email: nil, password: nil } }
      expect(response).to render_template :new
    end
  end

  describe 'GET #destroy' do
    it 'returns http success' do
      get :destroy
      expect(response).to redirect_to root_path
    end
  end
end
