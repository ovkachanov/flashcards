module CheckHelper
  def state_setting_and_check(obj,value_box = 0,value_check_attempt = 0, answer = 'Стол'  )
    obj.box = value_box
    obj.check_attempt = value_check_attempt
    obj.save
    fill_in 'answer', with: answer
    click_button 'Проверить'
    obj.reload
  end
end
