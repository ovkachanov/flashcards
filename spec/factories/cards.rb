require 'factory_bot'

FactoryBot.define do
  factory :card do
    original_text { "Table" }
    translated_text { "Стол" }
    review_date  { Time.now }
    photo { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/myfile/table.jpg'), 'image/jpeg') }
    deck
  end
end
