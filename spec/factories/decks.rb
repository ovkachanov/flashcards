require 'factory_bot'

FactoryBot.define do
  factory :deck do
    name { "MyTestDeck" }
    user
  end
end
