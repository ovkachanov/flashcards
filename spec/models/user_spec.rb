require 'rails_helper'

describe User do

  it "User is created correctly" do
    expect(build(:user)).to be_valid
  end

  describe "various input options" do
    let(:user) { create(:user) }

    context 'when email is nil' do
      let(:email) { nil }

      it { is_expected.not_to be_valid }
    end

    context 'when email is empty' do
      let(:email) { " " }

      it { is_expected.not_to be_valid }
    end

    context 'when password is nil' do
      let(:password) { nil }

      it { is_expected.not_to be_valid }
    end

    context 'when password is empty' do
      let(:password) { " " }

      it { is_expected.not_to be_valid }
    end
  end
end
