require 'rails_helper'

describe Card do

 describe "#check_answer" do
   let!(:card) { create(:card) }

   context 'levenshtein' do
     it "user answered with 100% match" do
       card.levenshtein('Стол')
       card.correct_answer_logic

       expect(card.box).to eq 1
       expect(card.review_date).to be_within(1.second).of(Time.now + 12.hours)
     end

     it "the user answered with one typo" do
       card.levenshtein('Стул')
       card.correct_answer_logic

       expect(card.box).to eq 1
       expect(card.review_date).to be_within(1.second).of(Time.now + 12.hours)
     end

     it "user answered with two typos" do
       card.levenshtein('Столп')
       card.correct_answer_logic

       expect(card.box).to eq 1
       expect(card.review_date).to be_within(1.second).of(Time.now + 12.hours)
     end

     it "maximum possible fifth basket" do
       card.box = 5
       card.levenshtein('Стол')
       card.correct_answer_logic

       expect(card.box).to eq 5
       expect(card.review_date).to be_within(1.second).of(Time.now + 1.year)
     end
    end

    context '#wrong_answer_logic' do
      it "the user answered with more than two typos" do
        card.levenshtein('Стулпн')
        card.wrong_answer_logic

        expect(card.box).to eq 0
        expect(card.check_attempt).to eq 1
      end
    end
  end

  describe "#set_review_date" do
   let (:card) { create(:card) }

   it "correct set review date" do
     expect(card.review_date.strftime("%d-%m-%Y")).to eq (Time.now.strftime("%d-%m-%Y"))
   end
 end


  describe "various input options" do
    let(:card) { build(:card, original_text: original_text, translated_text: translated_text) }
    let(:original_text) { 'original_text' }
    let(:translated_text) { 'translated_text' }

    context 'when original text is nil' do
      let(:original_text) { nil }

      it { is_expected.not_to be_valid }
    end

    context 'when original text is empty' do
      let(:original_text) { " " }

      it { is_expected.not_to be_valid }
    end

    context 'when translated text is nil' do
      let(:translated_text) { nil }

      it { is_expected.not_to be_valid }
    end

    context 'when translated text is empty' do
      let(:translated_text) { " " }

      it { is_expected.not_to be_valid }
    end

    context 'the original text and the translated text are equal - UpCase' do
      let(:original_text) { "TABLE" }
      let(:translated_text) { "TABLE" }

      it { is_expected.not_to be_valid }
    end

    context 'the original text and the translated text are equal - LowerCase' do
      let(:original_text) { "table" }
      let(:translated_text) { "table" }

      it { is_expected.not_to be_valid }
    end

    context 'the original text and the translated text are equal -MixedCase' do
      let(:original_text) { "TaBle" }
      let(:translated_text) { "TaBle" }

      it { is_expected.not_to be_valid }
    end
  end
end
