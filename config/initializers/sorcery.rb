Rails.application.config.sorcery.submodules = [:external]

  Rails.application.config.sorcery.configure do |config|

    config.external_providers = [:github]

    config.github.key = ENV['GITHUB_KEY']
    config.github.secret = ENV['GITHUB_SECRET_KEY']
    if Rails.env.production?
      config.github.callback_url = "https://myflashcardsapp.herokuapp.com/oauth/callback?provider=github"
    else
      config.github.callback_url = "http://0.0.0.0:3000/oauth/callback?provider=github"
    end
    config.github.user_info_mapping = { email: "name" }
    config.github.scope = ""

    # config.linkedin.key = ENV["LINKEDIN_CLIENT_ID"]
    # config.linkedin.secret = ENV["LINKEDIN_SECRET_KEY"]
    # config.linkedin.callback_url = "http://0.0.0.0:3000/oauth/callback?provider=linkedin"
    # config.linkedin.user_info_fields = ['user_id', 'first-name', 'last-name']
    # config.linkedin.user_info_mapping = {user_id: "ID", first_name: "firstName", last_name: "lastName"}
    # config.linkedin.access_permissions = ['r_liteprofile']

    config.user_config do |user|
      user.authentications_class = Authentication
    end
     config.user_class = 'User'
end
