Rails.application.routes.draw do
  get 'oauths/oauth'
  get 'oauths/callback'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'

  resources :cards do
    member do
      post 'check_translation', to: "cards#check_translation"
    end
  end

  resources :user_sessions
  resources :users

  resources :decks do
    member do
      patch 'make_current', to: "decks#make_current"
    end
  end

  get 'login' => 'user_sessions#new', :as => :login
  post 'logout' => 'user_sessions#destroy', :as => :logout

  post "oauth/callback" => "oauths#callback"
  get "oauth/callback" => "oauths#callback" # for use with Github, Facebook
  get "oauth/:provider" => "oauths#oauth", :as => :auth_at_provider
end
