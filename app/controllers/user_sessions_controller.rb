class UserSessionsController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = login(params[:email], params[:password])

    if @user
      redirect_back_or_to(@user, notice: 'Вход выполнен успешно!')
    else
      flash.now[:alert] = 'Неуспешно!'
      render action: 'new'
    end
  end

  def destroy
    logout
    redirect_to root_path, notice: 'Выход выполнен!'
  end
end
