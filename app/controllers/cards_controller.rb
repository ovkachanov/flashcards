class CardsController < ApplicationController
  before_action :set_card, only: [:show, :edit, :update, :destroy, :check_translation]

  def index
    @cards = Card.all_user_cards(current_user.decks)
  end

  def new
    @card = Card.new
  end

  def show
  end

  def edit
  end

  def create
    @card = Card.new(card_params)

    if @card.save
      redirect_to @card, :notice => "Карточка успешно создана!"
    else
      render 'new'
    end
  end

  def update
    if @card.update(card_params)
      redirect_to @card
    else
      render 'edit'
    end
  end

  def destroy
    @card.destroy if current_user.author_of?(@card.deck)
    redirect_to cards_path
  end

  def check_translation
    levenshtein = @card.levenshtein(params[:answer])
    if levenshtein == 0
      @card.correct_answer_logic
      flash[:notice] = "Правильно"
    elsif levenshtein.between?(1,2)
      @card.correct_answer_logic
      flash[:notice] = "Вы допустили опечатки, но Ваш ответ засчитан. Подробности ниже."
      flash[:user_answer] = "Ваш ответ: #{(params[:answer])}"
      flash[:correct_anser] = "Правильный ответ: #{@card.translated_text}"
    else
      @card.wrong_answer_logic
      flash[:notice] = "Не правильно"
    end
    redirect_to root_path
  end

private
  def set_card
    @card = Card.find(params[:id])
  end

  def card_params
    params.require(:card).permit(:original_text, :translated_text, :review_date, :photo, :remote_photo_url, :deck_id)
  end
end
