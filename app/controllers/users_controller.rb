class UsersController < ApplicationController
  before_action :set_user, only: [:show, :destroy, :edit, :update]

  def show
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      auto_login(@user)
      redirect_to root_url, :notice => "Вы зарегистрировались"
    else
      render :new
    end
  end

  def update
    if @user.update(user_params)
      redirect_to @user, :notice => "Данные успешно обновлены!"
    else
      render 'edit'
    end
  end

  def destroy
    @user.destroy
    redirect_to root_path, :notice => "Аккаунт удален"
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
