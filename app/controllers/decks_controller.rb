class DecksController < ApplicationController
  before_action :set_deck, only: [:show, :update, :edit, :make_current]

  def index
    @decks = current_user.decks.all
  end

  def new
    @deck = Deck.new
  end

  def show
  end

  def create
    @deck = Deck.new(deck_params.merge({ user: current_user }))

    if @deck.save
      redirect_to @deck, :notice => "Колода успешно создана!"
    else
      render 'new'
    end
  end

  def update
    if @deck.update(deck_params)
      redirect_to decks_path
    else
      render 'edit'
    end
  end

  def make_current
    current_user.update(current_deck_id: @deck.id) if current_user.author_of?(@deck)
    redirect_to decks_path
  end

private
  def set_deck
    @deck = Deck.find(params[:id])
  end

  def deck_params
    params.require(:deck).permit(:name)
  end
end
