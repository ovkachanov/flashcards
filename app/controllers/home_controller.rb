class HomeController < ApplicationController
  def index
    if current_user.present?
      @card = if current_user.current_deck.present?
        current_user.current_deck.cards.ready_to_review.last
      else
        Card.all_user_cards(current_user.decks).ready_to_review.last
      end
    end
  end
end
