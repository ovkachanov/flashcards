class Card < ApplicationRecord
  REVIEWTIME_AND_BOX = {
    1 => 12.hours,
    2 => 3.days,
    3 => 1.week,
    4 => 2.weeks,
    5 => 1.month,
  }

  belongs_to :deck

  validates :original_text, :translated_text, :deck, presence: true
  validate :check_correct_word, on: [ :create, :update ]

  scope :ready_to_review, -> { where('review_date <= ?', Time.now).order(Arel.sql("RANDOM()"))  }
  scope :all_user_cards, -> (decks) {where(deck: decks) }

  before_save :set_review_date

  mount_uploader :photo, PhotoUploader

  def set_review_date
    self.review_date = Time.now if new_record?
  end

  def check_correct_word
    if self.original_text.blank? || self.translated_text.blank?
      return
    elsif self.original_text.downcase == self.translated_text.downcase
      errors.add(:card, "Перевод и оригинальный текст не могут быть равны!")
    end
  end

  def levenshtein(answer)
    DamerauLevenshtein.distance(translated_text.downcase, answer.downcase)
  end

  def correct_answer_logic
    if self.box <= 4
      update(box: self.box += 1, review_date: Time.now + REVIEWTIME_AND_BOX[self.box])
    else
      update(review_date: Time.now + 1.year)
    end
  end

  def wrong_answer_logic
    update(check_attempt: self.check_attempt += 1)
    if self.check_attempt == 4
      update(box: 1, check_attempt: 0, review_date: Time.now + REVIEWTIME_AND_BOX[self.box])
    end
    false
  end
end
