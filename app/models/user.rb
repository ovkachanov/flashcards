class User < ApplicationRecord
  authenticates_with_sorcery!

  validates :password, length: { minimum: 3 }, if: -> { new_record? || changes[:crypted_password] }
  validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] }
  validates :email, uniqueness: true

  has_many :authentications, :dependent => :destroy
  has_many :decks
  belongs_to :current_deck, class_name: 'Deck', optional: true

  scope :unverified_user_cards, -> { joins(decks: :cards).where('review_date <= ?', Time.now) }

  accepts_nested_attributes_for :authentications

  def author_of?(object)
    id == object.user_id
  end
end
