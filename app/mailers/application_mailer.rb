class ApplicationMailer < ActionMailer::Base
  default from: 'myflashcardsapp.herokuapp.com'
  layout 'mailer'
end
