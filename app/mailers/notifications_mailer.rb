class NotificationsMailer < ApplicationMailer
  def pending_cards
    @users = User.unverified_user_cards
    @users.each do |user|
      mail(to: user.email, subject: "Напоминание о непроверенных карточках!")
    end
  end
end
