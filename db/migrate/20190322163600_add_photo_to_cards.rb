class AddPhotoToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :photo, :string
  end
end
