class AddAndRemoveColumns < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :current_deck_id, :integer
    remove_column :cards, :user_id
  end
end
