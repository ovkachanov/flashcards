class DeleteRemotePhotoUrlToCards < ActiveRecord::Migration[5.2]
  def change
    remove_column :cards, :remote_photo_url
  end
end
