class AddCheckAttemptAndBoxColumnsToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :check_attempt, :integer, default: 0
    add_column :cards, :box, :integer, default: 0
  end
end
