class AddDeckReferencesToCards < ActiveRecord::Migration[5.2]
  def change
    add_reference :cards, :deck, foreign_key: true, index: true
  end
end
