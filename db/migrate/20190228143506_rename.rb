class Rename < ActiveRecord::Migration[5.2]
  def change
    rename_column :cards, :translated_test, :translated_text
  end
end
