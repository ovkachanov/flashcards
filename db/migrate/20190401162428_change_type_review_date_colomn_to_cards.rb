class ChangeTypeReviewDateColomnToCards < ActiveRecord::Migration[5.2]
  def change
    change_column :cards, :review_date, :datetime
  end
end
