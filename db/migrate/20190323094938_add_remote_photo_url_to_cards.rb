class AddRemotePhotoUrlToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :remote_photo_url, :string
  end
end
