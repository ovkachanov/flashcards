doc = Nokogiri::HTML(open("http://1000mostcommonwords.com/1000-most-common-german-words/"))

class CardParser
  def self.parse(file)
    file.xpath('//*[@id="post-115"]/div/table/tbody/tr').each_with_object([]) do |word, words|
      words << [word.css('td')[1].text, word.css('td')[2].text]
    end
  end
end
